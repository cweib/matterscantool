<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>Matter-Scan-Tool V2.0.1 更新日志</title>
    <style type="text/css">
        body {
            font-family: '黑体';
            font-size: 12pt;
            font-weight: 400;
            font-style: normal;
        }
        p {
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
    <p>Matter-Scan-Tool-V2.0.1</p>
    <p>更新日志</p>
    <p>1、新增软件更新功能</p>
    <p>2、新增开关与灯具的测试选项</p>
    <p>3、修复Bugs</p>
</body>
</html>
